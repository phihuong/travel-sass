<?php
define('DB_NAME', 'travelDB');
define('DB_USER', 'root');
define('DB_PASSWORD', 'abcd@1234');
define('DB_HOST', '10.10.10.224:3306');
define('DB_CHARSET', 'utf8');


define('DB_COLLATE', '');
define('AUTH_KEY',         'eafacf8b52bc49ba7c7e6ece19d30e8e7f87359e');
define('SECURE_AUTH_KEY',  '143879226317e72ff8797294ec11353339c986dd');
define('LOGGED_IN_KEY',    '3cbf7c836ad5641272b29d740317808fc9855c06');
define('NONCE_KEY',        '1ecb9e91557e389a43f79e305802ceabfd391574');
define('AUTH_SALT',        'be21835a16e411594bbf561f6e66691d81a89307');
define('SECURE_AUTH_SALT', 'fd832143ad0c8f7c6419770efe6c05c962e8476f');
define('LOGGED_IN_SALT',   '8ea02bdad5daa32becd47f112ae342a924413d4a');
define('NONCE_SALT',       '53c26d97165d6f13e30e03b4c05c6f430ff4e78a');


$table_prefix = 'wp_';
define('WP_ALLOW_MULTISITE', true);

define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
define('COOKIE_DOMAIN', $_SERVER['HTTP_HOST']);
define('WP_DEBUG', false);



define('WP_MEMORY_LIMIT', '1024M');
define('WP_HTTP_BLOCK_EXTERNAL', true);
define( 'WP_POST_REVISIONS', 3 );




// If we're behind a proxy server and using HTTPS, we need to alert WordPress of that fact
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}


if (!defined('ABSPATH')) {
	define('ABSPATH', dirname(__FILE__) . '/');
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
