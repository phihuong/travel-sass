<?php

/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package Gutenslider
 */
// Exit if accessed directly.
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !function_exists( 'eedee_gutenslider_activation' ) ) {
    /**
     * Check if free / pro version is active on activation and deactivate in case
     */
    function eedee_gutenslider_activation()
    {
        if ( is_plugin_active( 'gutenslider/eedee-gutenslider.php' ) ) {
            deactivate_plugins( 'gutenslider/eedee-gutenslider.php' );
        }
        if ( is_plugin_active( 'gutenslider-premium/eedee-gutenslider.php' ) ) {
            deactivate_plugins( 'gutenslider-premium/eedee-gutenslider.php' );
        }
    }
    
    register_activation_hook( __FILE__, 'eedee_gutenslider_activation' );
}

if ( !function_exists( 'eedee_gutenslider_block_assets' ) ) {
    /**
     * Enqueue Gutenberg block assets for both frontend + backend.
     *
     * Assets enqueued:
     * 1. blocks.style.build.css - Frontend + Backend.
     * 2. blocks.build.js - Backend.
     * 3. blocks.editor.build.css - Backend.
     *
     * @uses {wp-blocks} for block type registration & related functions.
     * @uses {wp-element} for WP Element abstraction — structure of blocks.
     * @uses {wp-i18n} to internationalize the block's text.
     * @uses {wp-editor} for WP editor styles.
     * @since 1.0.0
     */
    function eedee_gutenslider_block_assets()
    {
        // phpcs:ignore
        include_once 'blocks/gutenslider/block-front.php';
        // Register block styles for both frontend + backend.
        wp_register_style(
            'eedee-gutenslider-style-css',
            // Handle.
            plugins_url( 'dist/gutenslider-blocks.style.build.css', dirname( __FILE__ ) ),
            // Block style CSS.
            array(),
            // Dependency to include the CSS after it.
            filemtime( plugin_dir_path( __DIR__ ) . 'dist/gutenslider-blocks.style.build.css' )
        );
        wp_register_style(
            'eedee-gutenslider-slick-css',
            // Handle.
            plugins_url( 'src/vendor/slick/slick.css', dirname( __FILE__ ) ),
            // Block style CSS.
            array(),
            // Dependency to include the CSS after it.
            filemtime( plugin_dir_path( __DIR__ ) . 'src/vendor/slick/slick.css' )
        );
        wp_register_style(
            'eedee-gutenslider-slick-theme-css',
            // Handle.
            plugins_url( 'src/vendor/slick/slick-theme.css', dirname( __FILE__ ) ),
            // Block style CSS.
            array(),
            // Dependency to include the CSS after it.
            filemtime( plugin_dir_path( __DIR__ ) . 'src/vendor/slick/slick-theme.css' )
        );
        wp_register_script(
            'eedee-gutenslider-slick-js',
            plugins_url( '/src/vendor/slick/slick.min.js', dirname( __FILE__ ) ),
            array( 'jquery' ),
            filemtime( plugin_dir_path( __DIR__ ) . '/src/vendor/slick/slick.min.js' ),
            true
        );
        wp_register_script(
            'eedee-gutenslider-js',
            plugins_url( '/dist/gutenslider.js', dirname( __FILE__ ) ),
            array( 'jquery', 'eedee-gutenslider-slick-js' ),
            filemtime( plugin_dir_path( __DIR__ ) . '/dist/gutenslider.js' ),
            true
        );
        // Register block editor script for backend.
        wp_register_script(
            'eedee-gutenslider-block-js',
            // Handle.
            plugins_url( '/dist/gutenslider-blocks.build.js', dirname( __FILE__ ) ),
            // Block.build.js: We register the block here. Built with Webpack.
            array(
                'wp-blocks',
                'wp-i18n',
                'wp-element',
                'wp-data'
            ),
            // Dependencies, defined above.
            filemtime( plugin_dir_path( __DIR__ ) . 'dist/gutenslider-blocks.build.js' ),
            // Version: filemtime — Gets file modification time.
            true
        );
        // Register block editor styles for backend.
        wp_register_style(
            'eedee-gutenslider-block-editor-css',
            // Handle.
            plugins_url( 'dist/gutenslider-blocks.editor.build.css', dirname( __FILE__ ) ),
            // Block editor CSS.
            array( 'wp-edit-blocks' ),
            // Dependency to include the CSS after it.
            filemtime( plugin_dir_path( __DIR__ ) . 'dist/gutenslider-blocks.editor.build.css' )
        );
        $editor_script = 'eedee-gutenslider-block-js';
        $scripts = [ 'eedee-gutenslider-slick-js', 'eedee-gutenslider-js' ];
        $eedee_gutenslider_block_variables = array(
            'siteUrl' => get_site_url(),
        );
        wp_localize_script( 'eedee-gutenslider-block-js', 'eedeeGutenslider', $eedee_gutenslider_block_variables );
        wp_localize_script( 'eedee-gutenslider-block-pro-js', 'eedeeGutenslider', $eedee_gutenslider_block_variables );
        if ( function_exists( 'register_block_type' ) ) {
            register_block_type( 'eedee/block-gutenslider', array(
                'attributes'      => array(
                'align'             => array(
                'type'    => 'string',
                'default' => 'none',
            ),
                'contentMode'       => array(
                'type'    => 'string',
                'default' => 'change',
            ),
                'bgImageId'         => array(
                'type' => 'number',
            ),
                'fadeMode'          => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'slidesToShow'      => array(
                'type'    => 'number',
                'default' => 1,
            ),
                'slidesToShowMd'    => array(
                'type'    => 'number',
                'default' => 1,
            ),
                'slidesToShowSm'    => array(
                'type'    => 'number',
                'default' => 1,
            ),
                'slidesToScroll'    => array(
                'type'    => 'number',
                'default' => 1,
            ),
                'slidesToScrollMd'  => array(
                'type'    => 'number',
                'default' => 1,
            ),
                'slidesToScrollSm'  => array(
                'type'    => 'number',
                'default' => 1,
            ),
                'sliderHeight'      => array(
                'type'    => 'string',
                'default' => '50vh',
            ),
                'sliderHeightMd'    => array(
                'type'    => 'string',
                'default' => '50vh',
            ),
                'sliderHeightSm'    => array(
                'type'    => 'string',
                'default' => '50vh',
            ),
                'isFullScreen'      => array(
                'type'    => 'boolean',
                'default' => false,
            ),
                'pauseOnHover'      => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'pauseOnFocus'      => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'pauseOnDotsHover'  => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'enableSpacing'     => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'paddingX'          => array(
                'type'    => 'string',
                'default' => '40px',
            ),
                'paddingXMd'        => array(
                'type'    => 'string',
                'default' => '40px',
            ),
                'paddingXSm'        => array(
                'type'    => 'string',
                'default' => '20px',
            ),
                'paddingY'          => array(
                'type'    => 'string',
                'default' => '40px',
            ),
                'paddingYMd'        => array(
                'type'    => 'string',
                'default' => '40px',
            ),
                'paddingYSm'        => array(
                'type'    => 'string',
                'default' => '20px',
            ),
                'spacingX'          => array(
                'type'    => 'number',
                'default' => 40,
            ),
                'spacingY'          => array(
                'type'    => 'number',
                'default' => 40,
            ),
                'spacingXMobile'    => array(
                'type'    => 'number',
                'default' => 40,
            ),
                'spacingYMobile'    => array(
                'type'    => 'number',
                'default' => 40,
            ),
                'spacingXTablet'    => array(
                'type'    => 'number',
                'default' => 40,
            ),
                'spacingYTablet'    => array(
                'type'    => 'number',
                'default' => 40,
            ),
                'spacingXDesktop'   => array(
                'type'    => 'number',
                'default' => 40,
            ),
                'spacingYDesktop'   => array(
                'type'    => 'number',
                'default' => 40,
            ),
                'spacingXUnit'      => array(
                'type'    => 'string',
                'default' => 'px',
            ),
                'spacingYUnit'      => array(
                'type'    => 'string',
                'default' => 'px',
            ),
                'autoplay'          => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'duration'          => array(
                'type'    => 'number',
                'default' => 3,
            ),
                'fadeSpeed'         => array(
                'type'    => 'number',
                'default' => 1,
            ),
                'arrows'            => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'arrowsMd'          => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'arrowsSm'          => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'arrowSize'         => array(
                'type'    => 'number',
                'default' => 20,
            ),
                'arrowSizeMd'       => array(
                'type'    => 'number',
                'default' => 20,
            ),
                'arrowSizeSm'       => array(
                'type'    => 'number',
                'default' => 20,
            ),
                'arrowColor'        => array(
                'type'    => 'string',
                'default' => '#ffffff',
            ),
                'arrowStyle'        => array(
                'type'    => 'string',
                'default' => 'arrow-style-1',
            ),
                'dots'              => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'dotsMd'            => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'dotsSm'            => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'dotSize'           => array(
                'type'    => 'number',
                'default' => 25,
            ),
                'dotSizeMd'         => array(
                'type'    => 'number',
                'default' => 25,
            ),
                'dotSizeSm'         => array(
                'type'    => 'number',
                'default' => 25,
            ),
                'dotColor'          => array(
                'type'    => 'string',
                'default' => '#ffffff',
            ),
                'dotStyle'          => array(
                'type'    => 'string',
                'default' => 'dot-style-1',
            ),
                'hasParallax'       => array(
                'type'    => 'boolean',
                'default' => false,
            ),
                'parallaxDirection' => array(
                'type'    => 'string',
                'default' => 'bottom',
            ),
                'parallaxAmount'    => array(
                'type'    => 'number',
                'default' => 1.3,
            ),
                'overlayColor'      => array(
                'type' => 'string',
            ),
                'dimRatio'          => array(
                'type'    => 'number',
                'default' => 0.5,
            ),
                'rgbaBackground'    => array(
                'type'    => 'string',
                'default' => '',
            ),
                'textColor'         => array(
                'type' => 'string',
            ),
                'mixBlendMode'      => array(
                'type'    => 'string',
                'default' => 'mb-none',
            ),
                'contentOpacity'    => array(
                'type'    => 'number',
                'default' => 1,
            ),
                'verticalAlign'     => array(
                'type'    => 'string',
                'default' => 'center',
            ),
                'loop'              => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'visibleOnDesktop'  => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'visibleOnTablet'   => array(
                'type'    => 'boolean',
                'default' => true,
            ),
                'visibleOnMobile'   => array(
                'type'    => 'boolean',
                'default' => true,
            ),
            ),
                'style'           => [
                'eedee-gutenslider-style-css',
                'eedee-gutenslider-slick-css',
                'eedee-gutenslider-slick-theme-css',
                'dashicons'
            ],
                'script'          => $scripts,
                'editor_script'   => $editor_script,
                'editor_style'    => 'eedee-gutenslider-block-editor-css',
                'render_callback' => 'eedee_gutenslider_dynamic_render_callback',
            ) );
        }
    }

}
if ( !function_exists( 'eedee_remove_max_srcset_image_width' ) ) {
    /**
     * Remove the max width filter for responsive images srcset
     *
     * @param number $max_width e max width (default 1600px).
     */
    function eedee_remove_max_srcset_image_width( $max_width )
    {
        return false;
    }

}
add_filter( 'max_srcset_image_width', 'eedee_remove_max_srcset_image_width' );
if ( !function_exists( 'eedee_gutenslider_load_textdomain' ) ) {
    /**
     * Load Plugin i18n
     */
    function eedee_gutenslider_load_textdomain()
    {
        load_plugin_textdomain( 'eedee-gutenslider', false, dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/' );
    }

}
add_image_size( 'xl', 1600, 9999 );
add_image_size( 'xxl', 2200, 9999 );
add_image_size( 'xxxl', 2800, 9999 );
add_image_size( 'xxxxl', 3400, 9999 );
add_image_size( 'xxxxxl', 4000, 9999 );
// Hook: Block assets.
add_action( 'init', 'eedee_gutenslider_block_assets' );
add_action( 'plugins_loaded', 'eedee_gutenslider_load_textdomain' );