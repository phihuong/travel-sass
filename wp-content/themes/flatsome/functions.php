<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */


/* Change the search slug to /search/
 * for the JSON+LD output in Yoast SEO
 */
function yst_change_json_ld_search_url() {
return trailingslashit( home_url() ) . 'search/{search_term_string}';
}
add_filter( 'wpseo_json_ld_search_url', 'yst_change_json_ld_search_url' );


