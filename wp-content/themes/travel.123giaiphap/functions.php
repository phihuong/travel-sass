<?php

add_action('wp_enqueue_scripts', 'enqueue_parent_styles');

function enqueue_parent_styles()
{
	wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
	wp_enqueue_style('wow-libs', get_stylesheet_directory() . '/node_modules/wowjs/css/libs/animate.css');
	wp_enqueue_style('wow-site', get_stylesheet_directory() . '/node_modules/wowjs/css/site.css');
	wp_enqueue_style('wow-js', get_stylesheet_directory() . '/node_modules/wowjs/dist/wow.js');
	wp_enqueue_style('main-js', get_stylesheet_directory() . '/js/main.js');
}

add_filter('woocommerce_loop_add_to_cart_link', 'wc_ninja_change_external_product_button', 15, 3);
function wc_ninja_change_external_product_button($button, $product, $args)
{
	$button_url  = $product->add_to_cart_url();
	$button_text = $product->add_to_cart_text();

	if ('external' === $product->get_type()) {
		$button_url  = $product->get_permalink();
	}

	return sprintf(
		'<a href="%s" data-quantity="%s" class="%s" %s>%s</a>',
		esc_url($button_url),
		esc_attr(isset($args['quantity']) ? $args['quantity'] : 1),
		esc_attr(isset($args['class']) ? $args['class'] : 'button'),
		isset($args['attributes']) ? wc_implode_html_attributes($args['attributes']) : '',
		esc_html($button_text)
	);
}



// To change add to cart text on single product page
add_filter('woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text');
function woocommerce_custom_single_add_to_cart_text()
{
	return __('Book Now', 'woocommerce');
}

// To change add to cart text on product archives(Collection) page
add_filter('woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text');
function woocommerce_custom_product_add_to_cart_text()
{
	return __('Learn More ', 'woocommerce');
}




add_shortcode('yoast-breadcrumbs', 'wpv_yoast_breadcrumbs');
function wpv_yoast_breadcrumbs($atts, $value)
{

	if (function_exists('yoast_breadcrumb')) {
		return yoast_breadcrumb("", "", false);
	}

	return '';
}


add_filter('searchwp_missing_integration_notices', '__return_false');

add_filter('woocommerce_single_product_carousel_options', 'ud_update_woo_flexslider_options');
function ud_update_woo_flexslider_options($options) {
      // show arrows
      $options['animation'] = "slide";

      // infinite loop
      $options['animationLoop'] = true;

      // autoplay (work with only slideshow too)
      $options['slideshow'] = true;
      $options['autoplay'] = true;
    
      return $options;
  }


  add_action('init', function() {
	pll_register_string('astra', 'In This Journey');
	pll_register_string('astra', 'About Tour');
	pll_register_string('astra', 'Other Tours');
  });

//   Turn off yoast auto redirect
  add_filter('wpseo_premium_post_redirect_slug_change', '__return_true' );
  add_filter('wpseo_premium_term_redirect_slug_change', '__return_true' );
  add_filter('wpseo_enable_notification_post_trash', '__return_false');
  add_filter('wpseo_enable_notification_post_slug_change', '__return_false');


