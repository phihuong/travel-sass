//Add Wowjs
wow = new WOW({
  animateClass: "animated",
  offset: 100,
  callback: function (box) {
    console.log("WOW: animating <" + box.tagName.toLowerCase() + ">");
  },
});
wow.init();
document.getElementById("moar").onclick = function () {
  var section = document.createElement("section");
  section.className = "section--purple wow fadeInDown";
  this.parentNode.insertBefore(section, this);
};
//End Wowjs

//Show Sidebar Filter
function myFunction() {
  document.getElementById("content").classList.toggle("show");
}
window.onclick = function (event) {
  if (!event.target.matches(".dropbtn")) {
    var dropdowns = document.getElementsByClassName("site-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains("show")) {
        openDropdown.classList.remove("show");
      }
    }
  }
};
//End Show Sidebar Filter
